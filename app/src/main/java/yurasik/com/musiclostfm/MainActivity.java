package yurasik.com.musiclostfm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import yurasik.com.musiclostfm.ui.general.TopArtistsFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.coordinator, new TopArtistsFragment())
                    .commitAllowingStateLoss();
        }

    }
}
