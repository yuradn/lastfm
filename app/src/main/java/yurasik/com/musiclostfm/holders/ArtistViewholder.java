package yurasik.com.musiclostfm.holders;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import yurasik.com.musiclostfm.R;
import yurasik.com.musiclostfm.model.Artist;
import yurasik.com.musiclostfm.utils.LogUtil;

/**
 * Created by Yurii on 5/18/17.
 */

public class ArtistViewholder extends RecyclerView.ViewHolder {

    TextView tvName;
    ImageView imageView;

    public ArtistViewholder(View itemView) {
        super(itemView);
        tvName = TextView.class.cast(itemView.findViewById(R.id.text_name));
        imageView = ImageView.class.cast(itemView.findViewById(R.id.image));
    }

    public void bind(Artist artist) {
        tvName.setText(getAdapterPosition()+": "+artist.getName());

        String path = null;
        for (int i = 0; i < artist.getImage().size(); i++) {
            path = artist.getImage().get(i).getText();
            if (!TextUtils.isEmpty(path)) break;
        }

        if (TextUtils.isEmpty(path)) return;

        Glide.with(tvName.getContext())
                .load(path)
                .into(imageView);
        LogUtil.info(this, "Bind position: " + getAdapterPosition());
    }
}
