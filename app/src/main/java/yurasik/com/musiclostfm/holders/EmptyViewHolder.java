package yurasik.com.musiclostfm.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import yurasik.com.musiclostfm.R;

public class EmptyViewHolder extends RecyclerView.ViewHolder{
    TextView tvName;

    public EmptyViewHolder(View itemView) {
        super(itemView);
        tvName = TextView.class.cast(itemView.findViewById(R.id.text_name));
    }

    public void bind(String message) {
        tvName.setText(message);
    }
}
