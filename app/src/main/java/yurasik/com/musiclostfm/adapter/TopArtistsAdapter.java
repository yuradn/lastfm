package yurasik.com.musiclostfm.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import yurasik.com.musiclostfm.R;
import yurasik.com.musiclostfm.holders.ArtistViewholder;
import yurasik.com.musiclostfm.holders.EmptyViewHolder;
import yurasik.com.musiclostfm.model.Artist;
import yurasik.com.musiclostfm.model.TopArtists;
import yurasik.com.musiclostfm.network.ApiManager;
import yurasik.com.musiclostfm.network.ResultCallback;

public class TopArtistsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private ArrayList<Artist> mArtists = new ArrayList<>();
    enum TYPE {EMPTY, ARTIST}
    private int page = 0;
    private boolean isRequest = false;

    public TopArtistsAdapter() {
        this.mArtists = new ArrayList<>();

        /*ApiManager.getProductReviews(getContext(), 50, 1, new ResultCallback<TopArtists>() {
            @Override
            public void success(TopArtists obj) {
                LogUtil.info(this, "success: " + obj);

                TopArtistsAdapter topArtistsAdapter =
                        new TopArtistsAdapter(obj.getArtists().getArtist());
                mRecyclerView.setAdapter(topArtistsAdapter);
            }
        });*/
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh = null;
        View v;
        switch (TYPE.values()[viewType]) {
            case EMPTY:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_empty, parent, false);
                vh = new EmptyViewHolder(v);
                vh.itemView.setOnClickListener(null);
                break;
            case ARTIST:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_artist, parent, false);
                vh = new ArtistViewholder(v);
                vh.itemView.setOnClickListener(this);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArtistViewholder) {
            ((ArtistViewholder)holder).bind(mArtists.get(position));
        } else {

        }

        if (!isRequest && (mArtists.size()==0 || position==mArtists.size()-1)) {
            page++;
            isRequest = true;
            ApiManager.getProductReviews(null, 30, page, new ResultCallback<TopArtists>() {
                @Override
                public void success(TopArtists obj) {
                    int previousSize = mArtists.size();
                    mArtists.addAll(obj.getArtists().getArtist());
                    /*notifyItemChanged(previousSize);
                    notifyItemRangeInserted(page, mArtists.size() - previousSize);*/
                    notifyDataSetChanged();
                    isRequest = false;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (!isRequest) {
            return mArtists.size()==0 ? 1 : mArtists.size();
        } else {
            return mArtists.size()+1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!isRequest) {
            return mArtists.size()==0 ? TYPE.EMPTY.ordinal() : TYPE.ARTIST.ordinal();
        } else {
            return position>=mArtists.size() ? TYPE.EMPTY.ordinal() : TYPE.ARTIST.ordinal();
        }
    }

    @Override
    public void onClick(View v) {

    }
}
