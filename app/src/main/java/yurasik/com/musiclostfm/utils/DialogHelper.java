package yurasik.com.musiclostfm.utils;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import yurasik.com.musiclostfm.R;
import yurasik.com.musiclostfm.dialog.LoadDataProgressDialog;
import yurasik.com.musiclostfm.interfaces.OnDialogEventInterface;

public class DialogHelper {
    private final static String TAG = "DialogHelper";

    public static void showDialog(FragmentManager supportFragmentManager, final String TAG, DialogFragment dialogFragment) {
        if (supportFragmentManager == null) return;
        FragmentTransaction ft = supportFragmentManager.beginTransaction();
        Fragment prev = supportFragmentManager.findFragmentByTag(TAG);
        if (prev != null) {
            ft.remove(prev);
        }

        // Create and show the dialog.
        dialogFragment.show(ft, TAG);
    }

    public static void dismissDialog(FragmentManager supportFragmentManager, final String TAG) {
        if (supportFragmentManager == null) return;
        Fragment dialog = supportFragmentManager.findFragmentByTag(TAG);
        if (dialog != null) {
            FragmentTransaction ft = supportFragmentManager.beginTransaction();
            ft.remove(dialog);
            ft.commitAllowingStateLoss();
        }
    }

    public static void showDialog(Context context, final String TAG, DialogFragment dialogFragment) {
        if (context == null) return;
        FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        Fragment prev = ((FragmentActivity) context).getSupportFragmentManager().findFragmentByTag(TAG);
        if (prev != null) {
            ft.remove(prev);
        }

        try {
            // Create and show the dialog.
            dialogFragment.show(ft, TAG);
        } catch (IllegalStateException e){
            e.printStackTrace();
        }
    }

    public static void dismissDialog(Context context, final String TAG) {
        if (context == null) return;
        Fragment dialog = ((FragmentActivity) context).getSupportFragmentManager().findFragmentByTag(TAG);
        if (dialog != null) {
            FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
            ft.remove(dialog);
            ft.commitAllowingStateLoss();
        }
    }

    public static void showProgressDialog(Context context, int textResId) {
        if (context == null) return;
        FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        Fragment prev = ((FragmentActivity) context).getSupportFragmentManager().findFragmentByTag(LoadDataProgressDialog.TAG);
        if (prev != null) {
            ft.remove(prev);
        }

        // Create and show the dialog.
        LoadDataProgressDialog loadDataProgressDialog = LoadDataProgressDialog.newInstance(textResId, true, false);
//        loadDataProgressDialog.show(ft, LoadDataProgressDialog.TAG);
        ft.add(loadDataProgressDialog, LoadDataProgressDialog.TAG);

        try {
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e){
        }
    }

    public static void showProgressDialog(Context context, int textResId, boolean indeterminate, long time) {
        if (context == null) return;
        FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        Fragment prev = ((FragmentActivity) context).getSupportFragmentManager().findFragmentByTag(LoadDataProgressDialog.TAG);
        if (prev != null) {
            ft.remove(prev);
        }

        // Create and show the dialog.
        LoadDataProgressDialog loadDataProgressDialog = LoadDataProgressDialog.newInstance(textResId, indeterminate, false, time);
//        loadDataProgressDialog.show(ft, LoadDataProgressDialog.TAG);
        ft.add(loadDataProgressDialog, LoadDataProgressDialog.TAG);
        ft.commitAllowingStateLoss();
    }

    public static void dismissProgressDialog(Context context) {
        if (context == null) return;
        Fragment dialog = ((FragmentActivity) context).getSupportFragmentManager().findFragmentByTag(LoadDataProgressDialog.TAG);
        if (dialog != null) {
            FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
            ft.remove(dialog);
            ft.commitAllowingStateLoss();
        }
    }

    public static boolean dismissProgressDialog(Context context, long timeRan) {
        if (context == null) return false;
        LoadDataProgressDialog dialog = (LoadDataProgressDialog)
                ((FragmentActivity) context)
                        .getSupportFragmentManager().findFragmentByTag(LoadDataProgressDialog.TAG);
        if (dialog != null && dialog.isMy(timeRan)) {
            FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
            ft.remove(dialog);
            ft.commitAllowingStateLoss();
            return true;
        }
        return false;
    }

    public static void showProgressDialog(Context context) {
        if (context == null) return;
        FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        Fragment prev = ((FragmentActivity) context).getSupportFragmentManager().findFragmentByTag(LoadDataProgressDialog.TAG);
        if (prev != null) {
            ft.remove(prev);
        }

        // Create and show the dialog.
        LoadDataProgressDialog loadDataProgressDialog = LoadDataProgressDialog.newInstance(R.string.loading, true, false);
//        loadDataProgressDialog.show(ft, LoadDataProgressDialog.TAG);
        ft.add(loadDataProgressDialog, LoadDataProgressDialog.TAG);
        ft.commitAllowingStateLoss();
    }

    public static void showProgressDialog(Context context, long time) {
        if (context == null) return;
        FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        Fragment prev = ((FragmentActivity) context).getSupportFragmentManager().findFragmentByTag(LoadDataProgressDialog.TAG);
        if (prev != null) {
            ft.remove(prev);
        }

        // Create and show the dialog.
        LoadDataProgressDialog loadDataProgressDialog =
                LoadDataProgressDialog.newInstance(R.string.loading, true, false, time);
//        loadDataProgressDialog.show(ft, LoadDataProgressDialog.TAG);
        ft.add(loadDataProgressDialog, LoadDataProgressDialog.TAG);
        ft.commitAllowingStateLoss();
    }

    /*public static void showTextDialogSomeTimeAndDismiss(Context context, int messageResourceId, int seconds) {
        showTextDialogSomeTimeAndDismiss(context, messageResourceId, null, seconds, null);
    }

    public static void showTextDialogSomeTimeAndDismiss(Context context, String message, int seconds) {
        showTextDialogSomeTimeAndDismiss(context, -1, message, seconds, null);
    }

    public static void showTextDialogSomeTimeAndDismiss(Context context, int messageResourceId, int seconds, final OnDialogEventInterface onDialogEventInterface) {
        showTextDialogSomeTimeAndDismiss(context, messageResourceId, null, seconds, onDialogEventInterface);
    }

    public static void showTextDialogSomeTimeAndDismiss(Context context, String message, int seconds, final OnDialogEventInterface onDialogEventInterface) {
        showTextDialogSomeTimeAndDismiss(context, -1, message, seconds, onDialogEventInterface);
    }*/

    /*private static void showTextDialogSomeTimeAndDismiss(final Context context, final int messageResourceId, final String message, final int seconds, final OnDialogEventInterface onDialogEventInterface) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                TextDialogFragment textDialogFragment;
                if (messageResourceId != -1) {
                    textDialogFragment = TextDialogFragment.newInstance(messageResourceId);
                } else {
                    textDialogFragment = TextDialogFragment.newInstance(message);
                }
                DialogHelper.showDialog(context, TextDialogFragment.TAG, textDialogFragment);
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    TimeUnit.SECONDS.sleep(seconds);
                } catch (InterruptedException e) {
                    LogUtil.error("Dialog InterruptedException", e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                DialogHelper.dismissDialog(context, TextDialogFragment.TAG);
                if (onDialogEventInterface != null) {
                    onDialogEventInterface.onDismissDialog();
                }
                super.onPostExecute(aVoid);
            }
        }.execute();
    }*/


}
