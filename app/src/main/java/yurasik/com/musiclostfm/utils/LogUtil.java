package yurasik.com.musiclostfm.utils;

import android.provider.SyncStateContract;
import android.util.Log;

import yurasik.com.musiclostfm.Constants;

/**
 * Created by Yurii on 5/18/17.
 */

public class LogUtil {
    public static void info(String message) {
        if (Constants.LOG_ENABLED) {
            Log.d("Last", message);
        }
    }

    public static void info(String tag, String message) {
        if (Constants.LOG_ENABLED) {
            Log.d(tag, message);
        }
    }

    public static void info(Object obj, String message) {
        if (Constants.LOG_ENABLED) {
            Log.d(obj.getClass().getSimpleName(), message);
        }
    }

    public static void error(String tag, String message) {
        if (Constants.LOG_ENABLED) {
            Log.e(tag, message);
        }
    }
}
