package yurasik.com.musiclostfm.ui.general;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import yurasik.com.musiclostfm.R;
import yurasik.com.musiclostfm.adapter.TopArtistsAdapter;
import yurasik.com.musiclostfm.network.ApiManager;
import yurasik.com.musiclostfm.network.ResultCallback;
import yurasik.com.musiclostfm.model.TopArtists;
import yurasik.com.musiclostfm.utils.LogUtil;

public class TopArtistsFragment extends Fragment {

    protected RecyclerView mRecyclerView;
    protected TopArtistsAdapter topArtistsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_to_artists, container, false);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        topArtistsAdapter = new TopArtistsAdapter();
        mRecyclerView.setAdapter(topArtistsAdapter);

        /*ApiManager.getProductReviews(getContext(), 50, 1, new ResultCallback<TopArtists>() {
            @Override
            public void success(TopArtists obj) {
                LogUtil.info(this, "success: " + obj);

                TopArtistsAdapter topArtistsAdapter =
                        new TopArtistsAdapter(obj.getArtists().getArtist());
                mRecyclerView.setAdapter(topArtistsAdapter);
            }
        });*/

    }
}
