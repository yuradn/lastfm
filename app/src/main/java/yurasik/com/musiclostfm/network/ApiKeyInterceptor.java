package yurasik.com.musiclostfm.network;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import yurasik.com.musiclostfm.BuildConfig;

public class ApiKeyInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl url = request.url().newBuilder()
                .addQueryParameter("api_key", BuildConfig.API_KEY)
                //.addQueryParameter("format", "json")
                .build();
        request = request.newBuilder().url(url).build();
        return chain.proceed(request);
    }
}
