package yurasik.com.musiclostfm.network;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import yurasik.com.musiclostfm.utils.LogUtil;


class LoggingInterceptor implements Interceptor {

    public LoggingInterceptor() {
        super();
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();

        long t1 = System.nanoTime();
        LogUtil.info(String.format("Sending request %s", request.url()));
        if (request.method().compareToIgnoreCase("post") == 0) {
            LogUtil.info(String.format("Body:%s", bodyToString(request)));
        }
        Response response = chain.proceed(request);

        long t2 = System.nanoTime();

        String bodyString = response.body().string();
        double timeOfExecution = ((t2 - t1) / 1e6d);
        LogUtil.info(String.format("Received response for %s in %.1fms%n%s",
                response.request().url(), timeOfExecution, bodyString));

//        if (Const.TRACK_API_CALLS) {
//            //tracking for API calls
//            Tracker tracker = ((AlloApplication) context).getDefaultTracker();
//            tracker.send(new HitBuilders.EventBuilder()
//                    .setCategory(Const.TRACK_API_CALLS_ACTION_NAME)
//                    .setAction(response.request().url().toString())
//                    .setLabel(Double.toString(timeOfExecution))
//                    .build());
//
//        }
//
        return response.newBuilder()
                .body(ResponseBody.create(response.body().contentType(), bodyString))
                .build();
    }

    public static String bodyToString(final Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}