package yurasik.com.musiclostfm.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import yurasik.com.musiclostfm.model.TopArtists;

public interface ApiService {
    /*Example URLs
    JSON: /2.0/?method=library.getartists&api_key=YOUR_API_KEY&user=joanofarctan&format=json
    XML: /2.0/?method=library.getartists&api_key=YOUR_API_KEY&user=joanofarctan

http://ws.audioscrobbler.com/2.0/?method=library.getartists&api_key=956b47edc4047ffdd468a3bc6ae1a2d4&user=joanofarctan&format=json

            Params
    user (Required) : The user whose library you want to fetch.
    limit (Optional) : The number of results to fetch per page. Defaults to 50.
    page (Optional) : The page number you wish to scan to.
    api_key (Required) : A Last.fm API key.*/
    /*@GET(Constants.REQUEST_METHOD+"{"+Constants.LIBRARY_ARTIST+"}"
            +"users/{user}/repos")*/

    @GET("?method=library.getartists&user=joanofarctan&format=json")
    Call<TopArtists> getTopAtrists(
            @Query("limit") int limit,
            @Query("page") int page
    );
}
