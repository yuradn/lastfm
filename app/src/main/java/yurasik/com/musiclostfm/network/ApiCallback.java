package yurasik.com.musiclostfm.network;

import android.content.Context;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import yurasik.com.musiclostfm.dialog.WarningDialog;
import yurasik.com.musiclostfm.utils.DialogHelper;
import yurasik.com.musiclostfm.utils.LogUtil;

public class ApiCallback<T> implements Callback<T> {
    private ResultCallback<T> resultCallback;
    private boolean canceled = true;
    private boolean dialog = true;
    private Context context;

    public ApiCallback(ResultCallback<T> resultCallback) {
        this(resultCallback, false);
    }

    public ApiCallback(ResultCallback<T> resultCallback, boolean canceled) {
        this(resultCallback, canceled, true);
    }

    public ApiCallback(ResultCallback<T> resultCallback, boolean canceled, boolean dialog) {
        this.resultCallback = resultCallback;
        this.canceled = canceled;
        this.dialog = dialog;
    }

    public void cancel(boolean remove) {
        canceled = true;
    }

    public boolean isCanceled() {
        return canceled;
    }


    private void catchError(String response, int code) {
        resultCallback.failure(null, response, code);
    }


    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (canceled) {
            resultCallback.onRequestCancelled();
            return;
        }
        if (response.isSuccessful()) {
            T obj = response.body();
            resultCallback.success(obj);
            if (dialog) {
                DialogHelper.dismissProgressDialog(context);
                context = null;
            }
        } else {
            int code = response.code();
            catchError(response.message(), code);
            ResponseBody errorBody = response.errorBody();
            LogUtil.error("ApiCallback onResponse ", errorBody.toString());
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (canceled) {
            resultCallback.onRequestCancelled();
            return;
        }

        if (dialog) {
            DialogHelper.dismissProgressDialog(context);
            DialogHelper.showDialog(context, "dd", new WarningDialog());
            return;
        }

        resultCallback.failure(t, null, -1);

        String s = t.toString();
        LogUtil.error("ApiCallback onFailure", s);
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
