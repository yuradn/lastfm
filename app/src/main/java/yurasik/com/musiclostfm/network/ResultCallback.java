package yurasik.com.musiclostfm.network;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import java.io.IOException;

import yurasik.com.musiclostfm.R;

public abstract class ResultCallback<T> {
    private Context context = null;

    public ResultCallback(Context context) {
        this.context = context;
    }

    public ResultCallback() {
    }

    public abstract void success(T obj);

    public void onRequestCancelled() {
        //empty method for overriding
    }

    public void clientError(String message) {
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    public void serverError(String message) {
        if (context != null) {
            if (TextUtils.isEmpty(message)) {
                Toast.makeText(context, R.string.textNetworkConnectionError, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        }

    }

    public void networkError(IOException e) {
        if (context != null) {
            Toast.makeText(context, R.string.textNetworkConnectionError, Toast.LENGTH_LONG).show();
        }
    }

    public void unexpectedError(Throwable t) {
        if (context != null) {
            Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void failure(Throwable t, String response, int code) {
        failureBase(t, response, code);
    }

    public final void failure(Context context, Throwable t, String response, int code) {
        this.context = context;
        failureBase(t, response, code);
    }

    private void failureBase(Throwable t, String response, int code) {
        if (t == null) {
            if (code >= 400 && code < 500) {
                clientError(response);
            } else if (code >= 500 && code < 600) {
                serverError(response);
            } else {
                unexpectedError(new RuntimeException("Unexpected response " + response));
            }
        } else {
            if (t instanceof IOException) {
                networkError((IOException) t);
            } else {
                unexpectedError(t);
            }
        }
    }
}