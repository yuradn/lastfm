package yurasik.com.musiclostfm.network;

import android.content.Context;

import retrofit2.Call;
import yurasik.com.musiclostfm.model.TopArtists;
import yurasik.com.musiclostfm.utils.DialogHelper;

public class ApiManager {

    public static ApiCallback<TopArtists> getProductReviews(Context context,
            int page, int pageSize, ResultCallback<TopArtists> resultCallback) {
        ApiCallback<TopArtists> apiCallback = new ApiCallback(resultCallback);
        apiCallback.setContext(context);
        Call<TopArtists> call = ApiFactory.getApiService().getTopAtrists(page, pageSize);
        call.enqueue(apiCallback);
        DialogHelper.showProgressDialog(context);
        return apiCallback;
    }

}
