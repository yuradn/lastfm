package yurasik.com.musiclostfm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Yurii on 5/18/17.
 */

public class Attr {

    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("page")
    @Expose
    private String page;
    @SerializedName("perPage")
    @Expose
    private String perPage;
    @SerializedName("totalPages")
    @Expose
    private String totalPages;
    @SerializedName("total")
    @Expose
    private String total;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Attr withUser(String user) {
        this.user = user;
        return this;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public Attr withPage(String page) {
        this.page = page;
        return this;
    }

    public String getPerPage() {
        return perPage;
    }

    public void setPerPage(String perPage) {
        this.perPage = perPage;
    }

    public Attr withPerPage(String perPage) {
        this.perPage = perPage;
        return this;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

    public Attr withTotalPages(String totalPages) {
        this.totalPages = totalPages;
        return this;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Attr withTotal(String total) {
        this.total = total;
        return this;
    }

    @Override
    public String toString() {
        return "Attr{" +
                "user='" + user + '\'' +
                ", page='" + page + '\'' +
                ", perPage='" + perPage + '\'' +
                ", totalPages='" + totalPages + '\'' +
                ", total='" + total + '\'' +
                '}';
    }
}