package yurasik.com.musiclostfm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Yurii on 5/18/17.
 */

public class TopArtists {

    @SerializedName("artists")
    @Expose
    private Artists artists;

    public Artists getArtists() {
        return artists;
    }

    public void setArtists(Artists artists) {
        this.artists = artists;
    }

    public TopArtists withArtists(Artists artists) {
        this.artists = artists;
        return this;
    }

    @Override
    public String toString() {
        return "TopArtists{" +
                "artists=" + artists +
                '}';
    }
}
