package yurasik.com.musiclostfm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Yurii on 5/18/17.
 */

public class Artists {

    @SerializedName("artist")
    @Expose
    private List<Artist> artist = null;
    @SerializedName("@attr")
    @Expose
    private Attr attr;

    public List<Artist> getArtist() {
        return artist;
    }

    public void setArtist(List<Artist> artist) {
        this.artist = artist;
    }

    public Artists withArtist(List<Artist> artist) {
        this.artist = artist;
        return this;
    }

    public Attr getAttr() {
        return attr;
    }

    public void setAttr(Attr attr) {
        this.attr = attr;
    }

    public Artists withAttr(Attr attr) {
        this.attr = attr;
        return this;
    }

    @Override
    public String toString() {
        return "Artists{" +
                "artist=" + artist +
                ", attr=" + attr +
                '}';
    }
}
