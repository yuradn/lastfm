package yurasik.com.musiclostfm.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import yurasik.com.musiclostfm.R;
import yurasik.com.musiclostfm.interfaces.ConnectionErrorInterface;

public class WarningDialog extends DialogFragment {

    public static final String TAG = WarningDialog.class.getSimpleName();

    private int messageId = R.string.app_name;
    private String messageString;
    private QuantityButtons quantityButtons = QuantityButtons.OneButton;
    private int resStringFirstButton = R.string.ok;
    private int resStringSecondButton = R.string.textButtonCancel;
    private boolean returnResultToFragment = true;

    //callback listener
    private DialogInterface.OnClickListener onClickListener = null;

    public enum QuantityButtons {
        OneButton,
        TwoButton
    }

    public WarningDialog() {
        //empty constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        deserializeArguments();
        if (getTargetFragment() != null && returnResultToFragment && getTargetFragment() instanceof ConnectionErrorInterface) {
            onClickListener = (DialogInterface.OnClickListener) getTargetFragment();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onClickListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onClickListener = null;
    }

    public static WarningDialog newInstance(Fragment targetFragment, int messageId) {
        return create(targetFragment, messageId, null, R.string.ok, R.string.textButtonCancel, QuantityButtons.OneButton, true);
    }

    public static WarningDialog newInstance(Fragment targetFragment, String message) {
        return create(targetFragment, 0, message, R.string.ok, R.string.textButtonCancel, QuantityButtons.OneButton, true);
    }

    public static WarningDialog newInstance(Fragment targetFragment, int messageId, boolean cancelable) {
        return create(targetFragment, messageId, null, R.string.ok, R.string.textButtonCancel, QuantityButtons.OneButton, cancelable);
    }

    public static WarningDialog newInstance(Fragment targetFragment, String message, boolean cancelable) {
        return create(targetFragment, 0, message, R.string.ok, R.string.textButtonCancel, QuantityButtons.OneButton, cancelable);
    }

    public static WarningDialog newInstance(Fragment targetFragment, String message, boolean cancelable, boolean returnResultToFragment) {
        return create(targetFragment, 0, message, R.string.ok, R.string.textButtonCancel, QuantityButtons.OneButton, cancelable, returnResultToFragment);
    }


    public static WarningDialog newInstance(Fragment targetFragment, int messageId, QuantityButtons quantityButtons, boolean cancelable) {
        return create(targetFragment, messageId, null, R.string.ok, R.string.textButtonCancel, quantityButtons, cancelable);
    }

    public static WarningDialog newInstance(Fragment targetFragment, int messageId, int resStringFirstButton, int resStringSecondButton, QuantityButtons quantityButtons, boolean cancelable) {
        return create(targetFragment, messageId, null, resStringFirstButton, resStringSecondButton, quantityButtons, cancelable);
    }

    public static WarningDialog newInstance(Fragment targetFragment, String messageString, int resStringFirstButton, int resStringSecondButton, QuantityButtons quantityButtons, boolean cancelable) {
        return create(targetFragment, 0, messageString, resStringFirstButton, resStringSecondButton, quantityButtons, cancelable);
    }

    public static WarningDialog newInstance(Fragment targetFragment, int messageId, String messageString, int resStringFirstButton, int resStringSecondButton, QuantityButtons quantityButtons, boolean cancelable) {
        return create(targetFragment, messageId, messageString, resStringFirstButton, resStringSecondButton, quantityButtons, cancelable);
    }

    public static WarningDialog newInstance(Fragment targetFragment, int messageId, String messageString, QuantityButtons quantityButtons, boolean cancelable) {
        return create(targetFragment, messageId, messageString, R.string.ok, R.string.textButtonCancel, quantityButtons, cancelable);
    }


    private static WarningDialog create(Fragment targetFragment, int messageId, String messageString, int resStringFirstButton, int resStringSecondButton, QuantityButtons quantityButtons, boolean cancelable, boolean returnResultToFragment) {

        Bundle bundle = serializeArgumentsToBundle(messageId, messageString, resStringFirstButton, resStringSecondButton, quantityButtons);
        WarningDialog warningDialog = new WarningDialog();
        bundle.putBoolean("returnResultToFragment", returnResultToFragment);
        if (targetFragment != null && returnResultToFragment) {
            /*parent fragment to back result

              Behavior by default:
               if(targetFragment == null) -> try to send callback to activity
               if(targetFragment != null) -> try to send callback to target fragment

              if you need to change this behavior or pass callback result to some other place - change returnResultToFragment param and process it in onAttach/onCreateView etc.
            */

            warningDialog.setTargetFragment(targetFragment, 0);
        }
        warningDialog.setArguments(bundle);
        warningDialog.setCancelable(cancelable);

        return warningDialog;
    }

    private static WarningDialog create(Fragment targetFragment, int messageId, String messageString, int resStringFirstButton, int resStringSecondButton, QuantityButtons quantityButtons, boolean cancelable) {

        Bundle bundle = serializeArgumentsToBundle(messageId, messageString, resStringFirstButton, resStringSecondButton, quantityButtons);
        WarningDialog warningDialog = new WarningDialog();
        if (targetFragment != null) {
            /*parent fragment to back result

              Behavior by default:
               if(targetFragment == null) -> try to send callback to activity
               if(targetFragment != null) -> try to send callback to target fragment

              if you need to change this behavior or pass callback result to some other place - change returnResultToFragment param and process it in onAttach/onCreateView etc.
            */

            bundle.putBoolean("returnResultToFragment", true);
            warningDialog.setTargetFragment(targetFragment, 0);
        }
        warningDialog.setArguments(bundle);
        warningDialog.setCancelable(cancelable);

        return warningDialog;
    }

    private static Bundle serializeArgumentsToBundle(int messageId, String messageString, int resStringFirstButton, int resStringSecondButton, QuantityButtons quantityButtons) {
        Bundle bundle = new Bundle();
        bundle.putInt("messageId", messageId);
        bundle.putString("messageString", messageString);
        bundle.putSerializable("quantityButtons", quantityButtons);
        bundle.putInt("resStringFirstButton", resStringFirstButton);
        bundle.putInt("resStringSecondButton", resStringSecondButton);
        return bundle;
    }

    private void deserializeArguments() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            messageId = bundle.getInt("messageId");
            messageString = bundle.getString("messageString");
            quantityButtons = (QuantityButtons) bundle.getSerializable("quantityButtons");
            resStringFirstButton = bundle.getInt("resStringFirstButton");
            resStringSecondButton = bundle.getInt("resStringSecondButton");
            returnResultToFragment = bundle.getBoolean("returnResultToFragment");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.warning_dialog_with_text_and_button_ok, null);


        TextView buttonFirst = (TextView) view.findViewById(R.id.buttonFirst);
        buttonFirst.setText(resStringFirstButton);
        TextView buttonSecond = (TextView) view.findViewById(R.id.buttonSecond);

        if (quantityButtons == QuantityButtons.TwoButton) {
            buttonSecond.setText(resStringSecondButton);
            buttonSecond.setVisibility(View.VISIBLE);
            buttonSecond.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().cancel();
                    if (onClickListener != null) {
                        onClickListener.onClick(WarningDialog.this.getDialog(), DialogInterface.BUTTON_NEGATIVE);
                    }
                }
            });
        } else {
            buttonSecond.setVisibility(View.GONE);
        }

        buttonFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (onClickListener != null) {
                    onClickListener.onClick(WarningDialog.this.getDialog(), DialogInterface.BUTTON_POSITIVE);
                }
            }
        });
        TextView message = (TextView) view.findViewById(R.id.message);
        if (messageString != null) {
            message.setText(messageString);
        } else {
            message.setText(messageId);
        }
        return view;
    }
}

