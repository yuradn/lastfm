package yurasik.com.musiclostfm.dialog;

import android.app.Dialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import yurasik.com.musiclostfm.R;


public class LoadDataProgressDialog extends DialogFragment {
    public static final String TAG = LoadDataProgressDialog.class.getSimpleName();

    public static boolean DIALOG_INDETERMINATE = true;
    public static boolean DIALOG_CANCELABLE = true;

    private final static String TIME_OF_RUN = "time_of_run";
    private final static String INDETERMINATE = "indeterminate";

    private long timeRan;
    private boolean dismiss = true;

    public LoadDataProgressDialog() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogThemeStyle);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                //do your stuff
                super.dismiss();
                if (dismiss) {
                    getActivity().onBackPressed();
                } else {
                    timeRan = 0;
                }
            }
        };
    }

    public static LoadDataProgressDialog newInstance() {
        return newInstance(R.string.loading);
    }

    public static LoadDataProgressDialog newInstance(int message) {
        return newInstance(message, DIALOG_INDETERMINATE, DIALOG_CANCELABLE);
    }

    public static LoadDataProgressDialog newInstance(int message, boolean indeterminate, boolean cancelable) {
        LoadDataProgressDialog loadDataProgressDialog = new LoadDataProgressDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("stringResource", message);
        bundle.putBoolean(INDETERMINATE, indeterminate);
        bundle.putBoolean("DIALOG_CANCELABLE", cancelable);
        loadDataProgressDialog.setArguments(bundle);
        return loadDataProgressDialog;
    }

    public static LoadDataProgressDialog newInstance(int message, boolean indeterminate, boolean cancelable, long time) {
        LoadDataProgressDialog loadDataProgressDialog = LoadDataProgressDialog.newInstance(message, indeterminate, cancelable);
        loadDataProgressDialog.getArguments().putLong(TIME_OF_RUN, time);
        loadDataProgressDialog.getArguments().putBoolean(INDETERMINATE, indeterminate);
        return loadDataProgressDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        timeRan = getArguments().getLong(TIME_OF_RUN, 0);
        dismiss = getArguments().getBoolean(INDETERMINATE, true);
        setCancelable(getArguments().getBoolean("DIALOG_CANCELABLE"));
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.load_data_progress_dialog, null);

        ProgressBar v = (ProgressBar) view.findViewById(R.id.loadingProgress);
        v.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent),
                PorterDuff.Mode.SRC_IN);

        TextView message = (TextView) view.findViewById(R.id.loadingDataDialogMessage);

        message.setText(getActivity().getString(getArguments().getInt("stringResource")));

        return view;
    }


    public boolean isMy(long timeRan) {
        return timeRan==this.timeRan;
    }
}
