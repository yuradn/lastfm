package yurasik.com.musiclostfm.interfaces;

public interface ConnectionErrorInterface {
    void ok();
    void cancel();
}
