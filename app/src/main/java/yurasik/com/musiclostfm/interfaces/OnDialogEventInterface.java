package yurasik.com.musiclostfm.interfaces;

public interface OnDialogEventInterface {
    void onDismissDialog();
}
