package yurasik.com.musiclostfm.app;

import android.app.Application;
import android.support.annotation.NonNull;

public class MusicLostApp extends Application{
    private static MusicLostApp sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        /*Picasso picasso = new Picasso.Builder(this)
                .downloader(new OkHttp3Downloader(this))
                .build();
        Picasso.setSingletonInstance(picasso);*/
    }

    @NonNull
    public static MusicLostApp getAppContext() {
        return sInstance;
    }
}
