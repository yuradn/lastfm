package yurasik.com.musiclostfm;

/**
 * Created by Yurii on 5/18/17.
 */

public class Constants {
    public static final String DEFAULT_SERVER_URL = "http://ws.audioscrobbler.com/2.0/";

    public static final String REQUEST_METHOD = "method";
    public static final String LIBRARY_ARTIST = "library.getartists";

    public static final boolean LOG_ENABLED = true;

    public static final String DATE_FORMAT = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'";
}
